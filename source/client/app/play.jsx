import React from 'react';
import PreparingMap from './components/gamePhases/prepare.jsx';
import cells from './components/mapCells.jsx';
import shipsForRendering from './shipPlacement.jsx';

 




class Play extends React.Component {

    render() {
        return (
            <div className="dashBoard">
                <PreparingMap cells = {cells}/>
            </div>
        );
    }
}


export default Play;

