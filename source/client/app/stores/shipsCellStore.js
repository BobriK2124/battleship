import { EventEmitter } from 'events';
import React from 'react'
import MyRect from '../Cell';
import dispatcher from "../dispatcher/dispatcher.js"
import mapStore from "./mapStore"

	var counter = 0;

class ShipsCellStore extends EventEmitter {
	constructor(){
		super()
		this.cellsOfShips = [];
		this.modelShips = [];
		this.isMyFleetDead = false;
	}

	damageShip(x,y){
		var props;
		var damagedCell;
		this.cellsOfShips.forEach(function(item){
			if(item.props.x == x && item.props.y ==y){
				props = {...item.props,color:'rgba(255, 51, 0,0.4)',strokeColor :'rgba(255, 51, 0,0.4)'};
			    damagedCell =item;
			}
		});
		var indexOfItem = this.cellsOfShips.indexOf(damagedCell);
		this.cellsOfShips.splice(indexOfItem,1);
		this.pushNewCell(<MyRect {...props}   key={props.x+"damaged"+props.y} />);	
		this.refresh();	
	}

	isAnyShipDead(){
		var countOfDamagedCell
		var step = 32; // я знаю,что не должно быть констант
		var ammountOfShips = this.modelShips.length;
		var ammountOfCells = this.cellsOfShips.length;
		for(var shipsCount = 0 ;shipsCount < ammountOfShips; shipsCount ++){
			if(this.modelShips[shipsCount].dead == false){
				var x = this.modelShips[shipsCount].x;
				var y = this.modelShips[shipsCount].y;
				var length = this.modelShips[shipsCount].length;
				
					for(var checkCount = 0;checkCount < length;checkCount++){
						countOfDamagedCell = 0;
						for(var cellsCounter = 0;cellsCounter < ammountOfCells;cellsCounter++){
						if(this.cellsOfShips[cellsCounter].props.y == y && this.cellsOfShips[cellsCounter].props.x == x && this.cellsOfShips[cellsCounter].props.color == "black"){
							countOfDamagedCell++;
						}
					}
				}
				if(countOfDamagedCell == length) {
					this.modelShips[shipsCount].dead = true;
					mapStore.deadArea(x,y,length);
				}
			}
		}
	}

	isFleetDead(){
		var binaryLogics = 0 ;
		this.cellsOfShips.forEach(function(item){
			if(item.props.color != "rgba(255, 51, 0,0.4)"){
				binaryLogics = 1;
			}
		});
		if(binaryLogics == 0) this.isMyFleetDead = true;
		this.refresh();
	}

	oponentHasLeave(){
		 this.isMyFleetDead = true;
		 this.refresh();
	}

	getStateOfMyFleet(){
		return this.isMyFleetDead;
	}

	getAllCellsOfShips(){
		return this.cellsOfShips;
	}

	getModels(){
		return this.modelShips;
	}

	refresh(){
		this.emit("change");
	}

	pushNewCell(item){
		this.cellsOfShips.push(item);
	}

	addNewModel(ship){
		this.modelShips.push(ship);
	}

	amIMiss(x,y){
		console.log(x,y);
		var is =0;
		this.cellsOfShips.forEach(function(item){
			if(item.props.x == x && item.props.y == y){
				is = 1;
			}
		});
		if(is == 0) return true;
		if(is ==1 ) return false;
	}



	viewModeOn(){
		var length = this.cellsOfShips.length;
		for(var count = 0;count < length; count++){
			var props = {...this.cellsOfShips[count].props,onClick:this.doNothingFunction.bind(this)};
			this.cellsOfShips.splice(count,1);
			this.pushNewCell(<MyRect {...props}   key={props.x+"view"+props.y} />);
		}
	}

	inverseShip(length,positionInCellsArray,positionInShipsArray , step){
		var stepCount = 1;
		var props;
		for(var count = 0;count < length; count ++){
			props = {...this.cellsOfShips[positionInCellsArray-1].props,onClick:this.doNothingFunction.bind(this)};
			this.cellsOfShips.splice(positionInCellsArray-1,1);
			this.pushNewCell(<MyRect {...props}  x={props.x + step*count} y={props.y - step*count}  key={props.x+"inversed"+props.y} />);
				
		}
			this.refresh();
	}
//
	doNothingFunction(){
		return;
	}


	handleDispatcher(action){
		switch(action.type){
			case "INVERSE":{
				this.inverseShip(action.payload.length,action.payload.indexOfShipInCellsArray,action.payload.indexOfShipInShipsArray,action.payload.step);
				break;
			}
			case "REFRESH":{
				this.refresh();
				break;
			}
			case "ADD_NEW_MODEL":{
				this.addNewModel(action.payload.item);
				break
			}
		}
	}
	

}

const shipsCellsStore = new ShipsCellStore;

dispatcher.register(shipsCellsStore.handleDispatcher.bind(shipsCellsStore));


export {dispatcher};
export default shipsCellsStore;



