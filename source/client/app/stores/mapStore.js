import { EventEmitter } from 'events';
import cells from '../components/mapCells.jsx'
import shipsCellsStore from './shipsCellStore'
import battlePannelCells from '../components/battlePannelCells.jsx'
import dispatcher from "../dispatcher/dispatcher.js"
import React from 'react'
import MyRect from '../Cell';

/*

				event-------dispatcher-----action---serverSide
				|				|
				|			action	
			store  -->view------|

*/
class MapStore extends EventEmitter {

	constructor(){
		super()
		this.cells = cells;
		this.aviableToPlaceCells = cells;
		this.battleMap = battlePannelCells;
		this.turn = "Preparing";
	}

	getAllCells(){
		return this.cells;
	}

	getAviableCells(){
		return this.aviableToPlaceCells;
	}

	getTurn(){
		return this.turn;
	}

	setTurn(turn){
		this.turn = turn;
		console.log(turn);
		this.battleMapRefresh();
	}

	getBattleMap(){
		return this.battleMap;
	}

	battleMapRefresh(){
		this.emit("refreshBattleMap");
	}

	fireOnCell(x,y){
		var length = this.cells.length;
		for(var count = 0;count<length;count++){
			if(this.cells[count].props.x == x && this.cells[count].props.y == y){
				var props = {...this.cells[count].props,color:"rgba(255, 255, 255,0.1)",strokeColor :"rgba(255, 255, 255,0.1)",clickable:false};
				this.cells.splice(count,1);
				this.cells.push(<MyRect {...props}   key={props.x+"fiered"+props.y} />);
			}
		}
		this.battleMapRefresh();
	}


	// deadArea(x,y,length){
	// 	console.log(x,y,length);
	// }

	fireOnCellOfBattleMap(x,y,color){
		var length = this.battleMap.length;
		for(var count = 0;count<length;count++){
			if(this.battleMap[count].props.x == x && this.battleMap[count].props.y == y){
				var props = {...this.battleMap[count].props,color:color,strokeColor :color,clickable:false};
				this.battleMap.splice(count,1);
				this.battleMap.push(<MyRect {...props}   key={props.x+"fiered"+props.y} />);
			}
		}
		this.battleMapRefresh();
	}


	handleDispatcher(action){
		switch(action.type){
			case "FIRE_ON_CELL":{
				console.log(action.payload);
				this.fireOnCellOfBattleMap(action.payload.x,action.payload.y,action.payload.color);
				break;
			}
		}
	}
}

var mapStore = new MapStore();
dispatcher.register(mapStore.handleDispatcher.bind(mapStore));


export {dispatcher};

 export default mapStore;