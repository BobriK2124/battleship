import React from 'react';
import {
    Layer,
    Rect,
    Stage,
    Group
} from 'react-konva';
import MyRect from './Cell'
import ships from './models/Ship'
import shipsCellsStore from "./stores/shipsCellStore.js"
import mapStore from './stores/mapStore.js'
import * as prepareActions from "./actions/prepareActions.js"

 


export function placeShips(x,y){
  if(!ships[0]) return;
  var length = ships[0].length;
  var xPos = x;
  var yPos = y;
  var position = 'begin';
  var ship = {length:length,x:xPos,y:yPos,dead:false};
  prepareActions.addNewModel(ship);
  for (var j = 0; j < length; j++) {
              var rectProps = {
                strokeColor : "rgba(230, 115, 0, 0.4)" ,
                clickable : true ,
                color : "rgba(230, 115, 0,0.4)",
                key : xPos+""+yPos ,
                x : xPos ,
                y : yPos,
                pos : position,
                inversed:false
              }
              shipsCellsStore.pushNewCell(
            < MyRect
              {...rectProps}
              onClick = {inverse.bind(this,rectProps)}
              />);
            position = 'b';
            yPos = yPos + 38;

          }
    ships.splice(0,1);    
    prepareActions.refresh();
    
}


function inverse(props){

var x = props.x;
var y =props.y;


var indexOfShipInShipsArray;
var indexOfShipInCellsArray;


var allCellsOfShips = shipsCellsStore.getAllCellsOfShips();
var shipsArray = shipsCellsStore.getModels();
var length;


 if(props.pos == 'begin'){
    
    shipsArray.forEach(function(item){
      if(x == item.x && y == item.y){
        indexOfShipInShipsArray = shipsArray.indexOf(item);
        length = item.length;
      }
    });

    allCellsOfShips.forEach(function(item){
       if(x == item.props.x && y == item.props.y){
          indexOfShipInCellsArray = allCellsOfShips.indexOf(item);
       }
    });
    prepareActions.inverse(length,indexOfShipInCellsArray + 1,indexOfShipInShipsArray,38);
 }


}



