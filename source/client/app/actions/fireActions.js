import dispatcher from "../dispatcher/dispatcher"


export function fire(xCoord,yCoord,color){
	console.log("I'm here");
	dispatcher.dispatch({
		type:"FIRE_ON_CELL",
		payload:{x:xCoord,y:yCoord,color:color}
	});
}