import dispatcher from "../dispatcher/dispatcher"


export function inverse(length,indexOfShipInCellsArray,indexOfShipInShipsArray,step){
	dispatcher.dispatch({
		type:"INVERSE",
		payload:{length:length,indexOfShipInCellsArray:indexOfShipInCellsArray,indexOfShipInShipsArray:indexOfShipInShipsArray,step:step}
	})
}

export function addNewModel(item){
	dispatcher.dispatch({
		type:"ADD_NEW_MODEL",
		payload:{item:item}
	})
}

export function getAviableCells(){
	 var a = dispatcher.dispatch({
		type:"GET_AVIABLE_CELLS"
	});
	 console.log(a);
	 return a;
}

export function refresh(){
	dispatcher.dispatch({
		type:"REFRESH"
	})
}