import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, Link, hashHistory} from 'react-router';
import Play from './play.jsx';
import BattleMap from "./components/gamePhases/battlePanel.jsx";
import StartScreen from "./components/gamePhases/startScreen.jsx";



class App extends React.Component {
    render() {
        return (

          <StartScreen />

        );
    }
}

ReactDOM.render((
    <Router history={hashHistory}>
        <Route path="/" component={App}/>
        <Route path="/p" component={Play}/>
        <Route path="/bm" component={BattleMap}/>
    </Router>

), document.getElementById('app'));
