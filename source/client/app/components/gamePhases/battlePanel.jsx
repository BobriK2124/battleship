import React from 'react';
import {
    Layer,
    Rect,
    Stage,
    Group
} from 'react-konva';
import {Link} from 'react-router';
import Konva from 'konva';
import mapStore from "../../stores/mapStore"
import shipsCellsStore from "../../stores/shipsCellStore"

class BattleMap extends React.Component {



  constructor(){
    super();
    this.state = {
      cells :mapStore.getAllCells(),
      ships:shipsCellsStore.getAllCellsOfShips(),
      battleMap:mapStore.getBattleMap(),
      gameOver:shipsCellsStore.getStateOfMyFleet(),
      turn:mapStore.getTurn()
    }
  }


  componentWillMount(){
    mapStore.on("refreshBattleMap",() => {
      this.setState({
        battleMap:mapStore.getBattleMap(),
        turn:mapStore.getTurn()
      })
    });
    shipsCellsStore.on("change",()=>{
      this.setState({
        ships:shipsCellsStore.getAllCellsOfShips(),
        gameOver:shipsCellsStore.getStateOfMyFleet()
      })
    });
  }

 

        render() {
          if(this.state.gameOver){
            return(
               <div id="gameOverDiv">
                    <h1 id="gameOver">GameOver</h1>
                </div>
              )
          }if(!this.state.gameOver){       


                 return ( <div>
                    <div id="state">
                    <h1 id="stateText">{this.state.turn}</h1>
                    </div>
                      
                    <div id = "infoFiels">
                    < Stage width = { 400 } height = { 400 } >
                    <Layer >
                       {this.state.cells}
                       </Layer>

                       <Layer >
                          {this.state.ships}
                          </Layer>

                        </Stage >
                         </div>
                         <div id="battleMap">
                            <Stage   width = { 400 } height = { 400 }>
                              <Layer>
                              {this.state.battleMap}
                              </Layer>
                            </Stage>
                         </div>
                    </div>

                 );
            }

        }

}
     

  export default BattleMap;
