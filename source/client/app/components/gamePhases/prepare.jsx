import React from 'react';
import {
    Layer,
    Rect,
    Stage,
    Group
} from 'react-konva';
import {Link} from 'react-router';
import Konva from 'konva';
import shipsForRendering  from  "../../shipPlacement.jsx";
import { placeShips } from "../../shipPlacement.jsx";
import mapStore from "../../stores/mapStore"
import shipsCellsStore from "../../stores/shipsCellStore"

class PreparingMap extends React.Component {



  constructor(){
    super();
    this.state = {
      cells :mapStore.getAllCells(),
      ships:shipsCellsStore.getAllCellsOfShips()
    }
  }


  componentWillMount(){
    shipsCellsStore.on("change",() => {
      this.setState({
        ships:shipsCellsStore.getAllCellsOfShips()
      })
    });
  }

  viewModeOn(){
    shipsCellsStore.viewModeOn();
  }



        render() {
             return ( <div>
                    <div id = "field">
                    < Stage width = { 400 } height = { 400 } >
                    <Layer >
                       {this.state.cells}
                       </Layer>

                       <Layer >
                          {this.state.ships}
                          </Layer>

                        </Stage >
                         </div>
                         <div id="confirmButton" >
                         <button className='b' onClick={this.viewModeOn} >  <Link to="/bm">Done</Link></button>
                         </div>
                    </div>
                 );
            }

        }


     

  export default PreparingMap;
