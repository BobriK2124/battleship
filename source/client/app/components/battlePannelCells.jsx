import React from 'react'
import MyRect from '../Cell';
import * as fireActions from "../actions/fireActions.js"
import socket from "../socket.js"
import shipsCellsStore from "../stores/shipsCellStore.js"
import mapStore from "../stores/mapStore.js";


var battlePannelCells = [];


socket.emit('register');
socket.on('setTurn',function(data){
  mapStore.setTurn(data.turn);
});

socket.on("fire",function(data){
  if(!shipsCellsStore.amIMiss(data.x,data.y)){
    shipsCellsStore.damageShip(data.x,data.y);
    socket.emit('hited',data);
  }

  if(shipsCellsStore.amIMiss(data.x,data.y)){
    socket.emit('missed',data);
      mapStore.fireOnCell(data.x,data.y);
  }
  shipsCellsStore.isFleetDead();
  if(  shipsCellsStore.isFleetDead()){
    socket.emit('leave');
  }
});

socket.on("leave",function(){
  shipsCellsStore.oponentHasLeave();
});

socket.on('hitShoot',function(data){
  console.log(data);
    fireActions.fire(data.x,data.y,data.color);
});


socket.on('miss',function(data){
  console.log(data);
    fireActions.fire(data.x,data.y,data.color);
});

function  fire(props){
  console.log(props.clickable);
    if(props.clickable){var x = props.x;
        var y = props.y;
        socket.emit("shoot",{x:x,y:y});
        }
  }


function createBattleMap() {

  for (var xCoord = 0; xCoord < 10; xCoord++) {
    for (var yCoord = 0; yCoord < 10; yCoord++) {
      var cellProps = {
        strokeColor :"rgba(26, 255, 255,0.2)"  ,
        key :  yCoord + 'bp' + xCoord ,
        clickable : true,
        color :  "rgba(26, 255, 255,0.2)"  ,
        x :  yCoord * 38 ,
        y :  xCoord * 38
      }
      battlePannelCells.push(
        < MyRect {...cellProps}
        onClick = {fire.bind(this,cellProps)}
            />);
      }
    }


}

createBattleMap();
export default battlePannelCells;
