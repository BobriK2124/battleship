import React from 'react'
import MyRect from '../Cell';
import { placeShips } from '../shipPlacement.jsx';

var cells = [];


function  place(props){
    var x = props.x;
    var y = props.y;
    placeShips(x,y);
  }


function createMap() {

  for (var xCoord = 0; xCoord < 10; xCoord++) {
    for (var yCoord = 0; yCoord < 10; yCoord++) {
      var cellProps = {
        strokeColor : "rgba(26, 255, 255,0.2)" ,
        key :  yCoord + '' + xCoord ,
        clickable : true,
        color :  "rgba(26, 255, 255,0.2)" ,
        x :  yCoord * 38 ,
        y :  xCoord * 38
      }
      cells.push(
        < MyRect {...cellProps}
        onClick = {place.bind(this,cellProps)}
            />);
      }
    }


}

createMap();
export default cells;
