import React from 'react';
import {
    Layer,
    Rect,
    Stage,
    Group
} from 'react-konva';


class MyRect extends React.Component {
    constructor(...props) {
        super(...props);
        this.state = {
            isInside: false,
            color: this.props.color,
            x: this.props.x,
            y: this.props.y,
            strokeColor: this.props.strokeColor,
            clickable: this.props.clickable

        };
        this.handleMouseInside = this.handleMouseInside.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);

}
    
    handleMouseInside() {
        this.setState({
            isInside: true
        });
    }
    handleMouseLeave() {
        this.setState({
            isInside: false
        });
    }

    render() {
        return ( <Rect
            x = { this.state.x }
            y = { this.state.y }
            width = { 34 }
            height = { 34 }
            fill = { this.state.color }
            clickable = { this.state.clickable }
            onClick = { this.props.onClick }
            shadowBlur = { 10 }
            stroke = { this.state.strokeColor }
            strokeWidth = { this.state.isInside ? 2 : 0 }
            onMouseEnter = { this.handleMouseInside }
            onMouseLeave = { this.handleMouseLeave }
             />

        );
    }
}



export default MyRect;
