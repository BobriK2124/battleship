import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "navigationBar": {
        "height": 30,
        "backgroundColor": "rgb(79, 87, 107)"
    },
    "body": {},
    "whary": {
        "paddingLeft": "40%",
        "paddingTop": "30%"
    },
    "label": {
        "paddingLeft": "40%",
        "fontFamily": "cursive",
        "fontStyle": "italic",
        "color": "white"
    },
    "dashBoard": {
        "paddingLeft": "10%"
    },
    "captan": {
        "fontFamily": "cursive",
        "fontStyle": "italic"
    },
    "whay": {
        "top": 50,
        "position": "absolute",
        "right": 150
    },
    "placementDiv": {
        "top": 120,
        "right": 50,
        "position": "absolute"
    },
    "field": {
        "top": 100,
        "position": "absolute"
    }
});