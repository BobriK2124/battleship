 const express = require('express');
 var app     = express();
 var server  = require('http').createServer(app);
 var io      = require('socket.io').listen(server);
 var path = require('path');


var users = [];
 const PORT = 3000;


 app.use(express.static(path.join(__dirname, '../client/public/')));

 app.get('/', function(req, res) {
     res.sendFile(__dirname + "/index.html");
 });




io.on('connection',function(socket){
		socket.on("shoot",function(data){
			if(socket.id == users[0].id && users[0].myTurn){
				io.to(users[1].id).emit('fire',data);
				users[0].myTurn = false;
				users[1].myTurn = true;
			}

			if(socket.id == users[1].id && users[1].myTurn){
				io.to(users[0].id).emit("fire",data);
				users[1].myTurn = false;
				users[0].myTurn = true;
			}

		});
		socket.on("register",function(){
			if(users.length < 2){
			var user = {};
			user.id = socket.id ;
			if(!users[0]){
				user.myTurn = true;
			}else{
				user.myTurn= false;
			}
			users.push(user);
		}
		if(users.length == 2){
			io.to(users[0].id).emit('setTurn',{turn:"Capitan,it is our turn to shoot"});
			io.to(users[1].id).emit('setTurn',{turn:"Capitan,enemy shells our ships"});
		}
		});

		socket.on('leave',function(){
			io.emit('leave');
		});
		socket.on("missed",function(data){
			if(socket.id == users[0].id && users[0].myTurn){
					io.to(users[0].id).emit('setTurn',{turn:"Capitan,it is our turn to shoot"});
			io.to(users[1].id).emit('setTurn',{turn:"Capitan,enemy shells our ships"});
				io.to(users[1].id).emit('miss',{x:data.x,y:data.y,color:"rgba(255, 255, 255,0.1)"});
			}

			if(socket.id == users[1].id && users[1].myTurn){
					io.to(users[1].id).emit('setTurn',{turn:"Capitan,it is our turn to shoot"});
			io.to(users[0].id).emit('setTurn',{turn:"Capitan,enemy shells our ships"});
				io.to(users[0].id).emit("miss",{x:data.x,y:data.y,color:"rgba(255, 255, 255,0.1)"});
			}

		});
		socket.on("hited",function(data){
			if(socket.id == users[0].id && users[0].myTurn){
				io.to(users[0].id).emit('setTurn',{turn:"Capitan,it is our turn to shoot"});
			    io.to(users[1].id).emit('setTurn',{turn:"Capitan,enemy shells our ships"});
				io.to(users[1].id).emit('hitShoot',{x:data.x,y:data.y,color:"rgba(51, 255, 51,0.4)"});
			}

			if(socket.id == users[1].id && users[1].myTurn){
				io.to(users[1].id).emit('setTurn',{turn:"Capitan,it is our turn to shoot"});
			io.to(users[0].id).emit('setTurn',{turn:"Capitan,enemy shells our ships"});
				io.to(users[0].id).emit("hitShoot",{x:data.x,y:data.y,color:"rgba(51, 255, 51,0.4)"});
			}

		});
	});

 
 


 server.listen(PORT, function() {
     console.log('there was magic at port :', PORT);
 });



