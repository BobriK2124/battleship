var webpack = require('webpack');
var path = require('path');

var BUID_DIR = path.resolve(__dirname,'source/client/public');
var APP_DIR = path.resolve(__dirname,'source/client/app');

var config = {
    entry:APP_DIR+'/app.js',
    output:{
      path:BUID_DIR,
      filename:'bundle.js'
    },
    module :{
      loaders:[
        {
        test:/\.jsx?$/,
        include:APP_DIR,
        loader:'babel'
      }
    ]
  }

};

module.exports = config;
